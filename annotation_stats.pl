#!/usr/bin/perl
#
#  annotation_stats
#
#  Created by Eduardo Andrés León on 2020-09-04.
#  Copyright (c) 2020 IPBLN. All rights reserved.
#
$|=1;

use strict;
use Getopt::Long;

my $help;
my $file;
my $pattern;
my $feature="gene";
my $column=9;

GetOptions(
	"help" => \$help,
	"file|fi=s" => \$file,
	"pattern|p=s" => \$pattern,
	"feature|f=s" => \$feature,
	"column|c=i" => \$column,
);

if($file and $pattern){
	#list and  arrays stars in 0, so we have to remove one place to the column
	$column--;
	#Open de gff file
	open(IN,$file) || die "$!: Can't read $file";
	#reading line by line
	my $number_of_lines=0;
	#number of found elements
	my $hits_number=0;
	#Asuming strange characters will be included as "+"
	$pattern=quotemeta($pattern);
	
	while(<IN>){
		
		#using only uncommented lines
		if($_ !~/^\#/){
			#removing intro at the end of the line
			chomp;
			#splitting fields by tab
			my @fields=split(/\t/);
			#For each "genomic feature" I have several lines
			#by defual I use only the "gene" feature for stats
			my $feature_file=$fields[2];
			if($feature_file eq $feature){
				#counting number of features for stats
				$number_of_lines++;
				my $important_field=$fields[$column];
				#Search for my pattern in the desc line
				if($important_field =~/$pattern/){
					$hits_number++;
				}
			}
		}
		
	}
	close IN;
	
	print "The pattern $pattern has been foun $hits_number in $number_of_lines: " . (($hits_number * 100) / $number_of_lines) ." %\n";
}else{
	
	help();
}

sub help{
	
my $usage = qq{
$0 

 Getting help:
     [--help]

 Needed parameters:
     [file] : gff/gft file      
     [pattern] : keyword to search for. Please use \" to quote terms.      

 Optional parameters:
     [feature] : Line to filter for. Each line has a feature keyword in the third column. Default: gene      
     [column] : For not standard gff/gtf use column to select the line to the search the pattern in. Default: column 9 
		 
 Examples:
		
    $0 -file TriTrypDB-26_LinfantumJPCM5.gff -pattern "carboxylase"
    $0 -fi TriTrypDB-26_LinfantumJPCM5.gff -pattern "hypothetical+protein"
    $0 -fi TriTrypDB-26_LinfantumJPCM5.gff -pattern "Ontology_term" -f mRNA
    $0 -fi TriTrypDB-26_LinfantumJPCM5.gff -pattern "Ontology_term" -f mRNA -c 9
			
};
	
print STDERR $usage;
exit();
	
}

