# Genomic differences in six T. cruzi strains belonging to DTUs that present differences in virulence and tissue tropism #

The six Trypanosoma cruzi discrete typing units (DTUs) present high genotypic heterogeneity and show phenotypic differences regarding host and tissue tropisms, and virulence. Here, the genomes of the B. M. L�pez (TcIa), Dm28 (TcId), Y (TcII), Ikiakarora (TcIII), SOL (TcV) and CL Brener (TcVI) strains, with different geographical origins and host/vectors have been sequenced, assembled and annotated. BUSCO analyses indicated a good quality for the assemblies that were used in comparative analyses of gene content, identification of Pr77-bearing retrotransposons and the study of the associations of mobile elements with relevant gene groups and other repetitive sequences. The results show differences among the six genomes in the copy number of genes related to virulence processes, the abundance of retrotransposons bearing the Pr77-hallmark and the presence of Pr77-containing sequences not associated with retroelements. The analyses also show frequent associations of Pr77-bearing retrotransposons and Pr77 hallmarks with genes coding for trans-sialidases, RHS, MASP or hypothetical proteins, showing variable proportion depending on the type of retroelement, gene class and parasite strain. These differences in the genomic distribution of active retroelements and other Pr77-containing elements have shaped the genome architecture of these six strains and might be contributing to the phenotypic variability existing among them. 

## Annotation statistics ##

**AnnotationStats** is a software designed to measure the information stored in a gff file from the TriTrypDB.

    AnnotationStats has been created at The Institute of Parasitology and Biomedicine "L�pez-Neyra" (IPBLN)
    Copyright (c) 2020 IPBLN. All rights reserved.

### 1. Pre-requisites ###

#### Perl ####
Perl (Practical Extraction and Reporting Language) can be installed in all kind of platforms:

* **Windows**. To install Perl in a windows machine, pelase follow this [steps](https://learn.perl.org/installing/windows.html).
* **Linux**. Perl is installed by default in all Unix machines.
* **Apple**. Perl is also installed by default in all Apple computers, but Xcode is required to install perl modules. Use this link to install [XCode](https://itunes.apple.com/es/app/xcode/id497799835?l=en&mt=12).

#### Perl modules####
AnnotationStats read parameters from the console. So a [pluglin](https://perldoc.perl.org/Getopt/Long.html) to plugin to perse this infromation is needed.
 This plugin can be installed in any kind of machines having perl by invoking the following command:
     
	 cpan Getopt::Long


### 2. Usage ###

    Getting help:
     [--help]

    Needed parameters:
     [file] : gff/gft file      
     [pattern] : keyword to search for. Please use " to quote terms.      

    Optional parameters:
     [feature] : Line to filter for. Each line has a feature keyword in the third column. Default: gene      
     [column] : For not standard gff/gtf use column to select the line to the search the pattern in. Default: column 9 
		 
    Examples:
		
    annotation_stats.pl -file TriTrypDB-26_LinfantumJPCM5.gff -pattern "carboxylase"
    annotation_stats.pl -fi TriTrypDB-26_LinfantumJPCM5.gff -pattern "hypothetical+protein"
    annotation_stats.pl -fi TriTrypDB-26_LinfantumJPCM5.gff -pattern "Ontology_term" -f mRNA
    annotation_stats.pl -fi TriTrypDB-26_LinfantumJPCM5.gff -pattern "Ontology_term" -f mRNA -c 9
	
### 3. Who do I talk to? ###
* mcthomas@ipb.csic.es, bioinformatica@ipb.csic.es